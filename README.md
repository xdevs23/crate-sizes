# sizes

A crate to format byte sizes.

## Example

```rust
use sizes::{BinarySize};
println!("{}", BinarySize::from(2_u64*1024*1024).rounded());
// Result: 2.00 MiB
```

```rust
use sizes:{BinarySize, YIB};
println!("{}", BinarySize::from(2_u128*YIB));
// Result: 2 YiB
```

