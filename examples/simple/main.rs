
use sizes::{BinarySize, YIB};

fn main() {
    println!("{}", BinarySize::from(2_u64*1024*1024).rounded());
    println!("{}", BinarySize::from(2_u128*YIB));
}

